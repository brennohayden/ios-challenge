Marvel iOS application written in Swift 5 using VIP with clean architecture concepts 🚀.

## Screenshots

## Requirements

- Swift 5.0
- Xcode 11.0+
- iOS 11.0+

## Used Libraries

* [**Kingfisher**](https://github.com/onevcat/Kingfisher)
* [**CryptoSwift**](https://github.com/krzyzanowskim/CryptoSwift)
* [**SwiftLint**](https://github.com/realm/SwiftLint)

##  Features 

- [x] List of Marvel characters.
- [x] Detail of a specific character.
- [x] List of favorite characters.
- [x] Persist favorite characters on the device so that they can be accessed offline
- [x] Search by name characters.
- [x] Accessibility
- [x] Internationalization (EN/PTBR)
- [x] Fastlane
- [x] xCov
- [x] Connection errors
- [x] Unit test


## Installation

* Download the project.
* Open the terminal and navigate to the directory of project ```cd ios-challenge```.
This project uses [Bundler](http://bundler.io) and [CocoaPods](https://cocoapods.org). All you need to setup it properly is:
```
bundle
bundle exec pod install
```
* Open the workspace ```open ios-challenge.xcworkspace```


## Tests And Coverage

You can run the tests any time. All your need to do is:
```
bundle exec fastlane test
```

## Improvments

- Implement UI Tests
- Improve layout
- Tracis-CI

## Author

* [**Brenno Hayden**](https://gitlab.com/brennohayden)

