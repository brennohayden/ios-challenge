//
//  MarvelPresentTests.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import UIKit
import XCTest
@testable import ios_challenge

class MarvelPresentTests: XCTestCase {
    var sut: MarvelPresent!
    var spy: MarvelViewControllerMock!
    
    override func setUp() {
        super.setUp()
        spy = MarvelViewControllerMock()
        sut = MarvelPresent(viewController: spy)
    }
    
    override func tearDown() {
        sut = nil
        spy = nil
        super.tearDown()
    }
    
    func testPresentCharacters() {
        sut.presentCharacters(itens: [CharacterViewModel]())
        XCTAssertTrue(spy.displayCharacters)
    }
}



   

   


