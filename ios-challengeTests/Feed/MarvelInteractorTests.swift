//
//  MarvelInteractorTests.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import UIKit
import XCTest
@testable import ios_challenge

class MarvelInteractorTests: XCTestCase {
    
    var sut: MarvelInteractor!
    var spy: MarvelPresentMock!
    var repository: MarvelRepositoryMock!
    
    override func setUp() {
        super.setUp()
        spy = MarvelPresentMock()
        sut = MarvelInteractor(presenter: spy)
        repository =  MarvelRepositoryMock()
        sut.repository = repository
    }
    override func tearDown() {
        sut = nil
        spy = nil
        super.tearDown()
    }
    
    func testFetch() {
        repository.shouldFailRequest = false
        sut.fetchCharacters(offset: 0, limit: 30)
        XCTAssertTrue(spy.presentCharacters)
        XCTAssertFalse(spy.presentError)
    }
    
    func testError() {
        repository.shouldFailRequest = true
        sut.fetchCharacters(offset: -1, limit: -30)
        XCTAssertFalse(spy.presentCharacters)
        XCTAssertTrue(!spy.presentError)
    }
}
