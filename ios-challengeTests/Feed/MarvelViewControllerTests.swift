//
//  MarvelViewControllerTests.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

@testable import ios_challenge
import XCTest

class MarvelViewControllerTests: XCTestCase {
    
    var sut: MarvelViewController!
    var spy: MarvelInteractorMock!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        sut = makeSUT()
        window = UIWindow()
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    override func tearDown() {
        window = nil
        sut = nil
        spy = nil
        super.tearDown()
    }
    
    func testViewDidLoad() {
        sut.viewDidLoad()
        XCTAssertTrue(spy.fetchCharacters)
    }
    
    func testViewDidAppear() {
        sut = makeSUT()
        sut.viewDidAppear(false)
        XCTAssertEqual(sut.title, nil)
    }
    
    func makeSUT() -> MarvelViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sut = storyboard.instantiateViewController(identifier: "MarvelViewController") as! MarvelViewController
        
        spy = MarvelInteractorMock(presenter: MarvelPresentMock())
        sut.interactor = spy
        
        return sut
    }
}
