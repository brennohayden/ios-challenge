
//
//  MarvelRepositoryMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import XCTest
@testable import ios_challenge

class MarvelRepositoryMock: MarvelRepositoryStoreProtocol {
    
    var shouldFailRequest: Bool = false
    
    func fetchCharacters(offset: Int, limit: Int,
                         onSuccess:@escaping (([Character])->Void),
                         onFailure:@escaping ((Error)->Void)) {
        
        if shouldFailRequest {
//            onFailure { (error ) in}
        } else {
            onSuccess([Character]())
        }
    }
}
