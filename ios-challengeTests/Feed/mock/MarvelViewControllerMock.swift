//
//  MarvelViewControllerMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import UIKit
import XCTest
@testable import ios_challenge

class MarvelViewControllerMock: MarvelViewControllerDisplayDelegate {
 
    var displayCharacters: Bool = false
    var displayError: Bool = false
    
    var itens: [CharacterViewModel]?
    var error: Error?
    
    func displayCharacters(itens: [CharacterViewModel]) {
        displayCharacters = true
        self.itens = itens
    }
    func displayError(error: Error) {
        displayError = true
        self.error = error
    }
}
