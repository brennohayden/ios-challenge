//
//  MarvelPresentMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import XCTest
@testable import ios_challenge

class MarvelPresentMock: MarvelPresentProtocol {
    
    var presentCharacters: Bool = false
    var presentError: Bool = false
    var itens: [CharacterViewModel]?
    var error: Error?
    
    func presentCharacters(itens: [CharacterViewModel]) {
        presentCharacters = true
        self.itens = itens
    }
    
    func presentError(error: Error) {
        presentError = true
        self.error = error
    }
}
