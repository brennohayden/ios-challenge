//
//  MarvelInteractorMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import XCTest
@testable import ios_challenge

class MarvelInteractorMock: MarvelInteractorProtocol {
    
    var fetchCharacters: Bool = false
    var offset: Int?
    var limit: Int?
    var presenter: MarvelPresentProtocol?
    
    init(presenter: MarvelPresentProtocol) {
        self.presenter = presenter
    }
    
    func fetchCharacters(offset: Int, limit: Int) {
        fetchCharacters = true
        self.offset = offset
        self.limit = limit
    }
}
