//
//  FavoritesViewControllerTests.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

@testable import ios_challenge
import XCTest

class FavoritesViewControllerTests: XCTestCase {
    
    var sut: FavoritesViewController!
    var spy: FavoritesInteractorMock!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        sut = makeSUT()
        window = UIWindow()
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    override func tearDown() {
        window = nil
        sut = nil
        spy = nil
        super.tearDown()
    }
    
    func testFetchCharacters() {
        sut.viewDidLoad()
        XCTAssertTrue(spy.fetchCharactersCalled)
    }
    
    func testViewDidAppear() {
        sut = makeSUT()
        sut.viewDidAppear(false)
        XCTAssertEqual(sut.title, nil)
    }
    
    func makeSUT() -> FavoritesViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sut = storyboard.instantiateViewController(identifier: "FavoritesViewController") as! FavoritesViewController
        
        spy = FavoritesInteractorMock(presenter: FavoritesPresenterMock())
        sut.interactor = spy
        
        return sut
    }
}
