//
//  FavoritesPresenterMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import XCTest
@testable import ios_challenge

class FavoritesPresenterMock: FavoritesPresenterProtocol {
    
    var presentCharacters: Bool = false
    var presentComics: Bool = false
    var itens: [CharacterViewModel]?
    var comics: [ComicViewModel]?
    
    func presentCharacters(itens: [CharacterViewModel]) {
        presentCharacters = true
        self.itens = itens
    }
    func presentComics(itens: [ComicViewModel]) {
        presentComics = true
        self.comics = itens
    }
}
