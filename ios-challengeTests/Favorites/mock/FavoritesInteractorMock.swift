//
//  FavoritesInteractorMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import XCTest
@testable import ios_challenge

class FavoritesInteractorMock: FavoritesInteractorProtocol {
    
    var fetchCharactersCalled: Bool = false
    var presenter: FavoritesPresenterProtocol?
    
    init(presenter: FavoritesPresenterProtocol) {
        self.presenter = presenter
    }
    
    func fetchCharacters() {
        fetchCharactersCalled = true
    }
    
}
