//
//  FavoritesRepositoryMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import XCTest
@testable import ios_challenge

class FavoritesRepositoryMock: FavoritesRepositoryStoreProtocol {
    
    var shouldCharacterRequest: Bool = false
    var shouldComicRequest: Bool = false
    
    func fetchCharacters(onSuccess: @escaping ([CharacterViewModel]) -> Void) {
        shouldCharacterRequest = true
        onSuccess([CharacterViewModel]())
    }
    
    func fetchComics(id: String, onSuccess: @escaping ([ComicViewModel]) -> Void) {
        shouldComicRequest = true
        onSuccess([ComicViewModel]())
    }
}
