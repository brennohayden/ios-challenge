//
//  FavoritesInteractorTests.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import XCTest
@testable import ios_challenge

class FavoritesInteractorTests: XCTestCase {
    
    var sut: FavoritesInteractor!
    var spy: FavoritesPresenterMock!
    var repository: FavoritesRepositoryMock!
    
    override func setUp() {
        super.setUp()
        spy = FavoritesPresenterMock()
        sut = FavoritesInteractor(presenter: spy)
        repository =  FavoritesRepositoryMock()
        sut.repository = repository
    }
    override func tearDown() {
        sut = nil
        spy = nil
        super.tearDown()
    }
    
    func testFetchCharacters() {
        repository.shouldCharacterRequest = false
        sut.fetchCharacters()
        
        XCTAssertTrue(spy.presentCharacters)
    }
}
