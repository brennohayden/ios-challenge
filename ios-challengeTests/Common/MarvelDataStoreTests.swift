//
//  MarvelDataStoreTests.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

@testable import ios_challenge
import XCTest

class MarvelDataStoreTests: XCTestCase {
    
    
    override func setUp() {
        super.setUp()
        RunLoop.current.run(until: Date())
    }
    
    override func tearDown() {
        super.tearDown()
    }
    func testLoadFAV() {
        let baseStored = MarvelDataStore.loadFAV()
       
        XCTAssertNotNil(baseStored)
        XCTAssertTrue(baseStored.count >=  0)
    }
    
    func testAddFAV() {
        let baseStored = MarvelDataStore.loadFAV()
        let model = CharacterViewModel(id: 12345, name: "Spider Man",
                                       description: "Aranha Humana", thumbnail: "http://", isFavorite: false)
        MarvelDataStore.addFAV(model: model)
        let afterStored = MarvelDataStore.loadFAV()
        XCTAssertTrue(baseStored.count + 1 ==  afterStored.count)
    }
    
    func testDelFAV() {
        let baseStored = MarvelDataStore.loadFAV()
        
        let model = CharacterViewModel(id: 54321, name: "Spider Man",
                                       description: "Aranha Humana", thumbnail: "http://", isFavorite: false)
        MarvelDataStore.addFAV(model: model)
        MarvelDataStore.delFAV(model: model)
        
        let afterStored = MarvelDataStore.loadFAV()
        
        XCTAssertTrue(baseStored.count ==  afterStored.count)
    }
    
    func testDelFAVError() {
        let baseStored = MarvelDataStore.loadFAV()
        
        let model = CharacterViewModel(id: 54321, name: "Spider Man",
                                       description: "Aranha Humana", thumbnail: "http://", isFavorite: false)
        MarvelDataStore.delFAV(model: model)
        
        let afterStored = MarvelDataStore.loadFAV()
        
        XCTAssertTrue(baseStored.count - 1 !=  afterStored.count)
    }
    
    func testLoadComic() {
        let baseStored = MarvelDataStore.loadComics()
        XCTAssertNotNil(baseStored)
        XCTAssertTrue(baseStored.count >=  0)
    }
    
    func testSaveComics() {
        let baseStored = MarvelDataStore.loadComics()
        let model = ComicViewModel(characterId: 444, id: 1, name: "Spider Comic",
                                   description: "vai", title: "Pandemia",
                                   thumbnail: "http://", isFavorite: true)
        MarvelDataStore.saveComics(model: model)
        let afterStored =  MarvelDataStore.loadComics()
        XCTAssertTrue(baseStored.count + 1 ==  afterStored.count)
    }
    
    func testSaveComicsNotSave() {
        let baseStored = MarvelDataStore.loadComics()
        let model = ComicViewModel(characterId: 444, id: 1, name: "Spider Comic",
                                   description: "vai", title: "Pandemia",
                                   thumbnail: "http://", isFavorite: true)
        MarvelDataStore.saveComics(model: model)
        let afterStored =  MarvelDataStore.loadComics()
        XCTAssertFalse(baseStored.count + 1 ==  afterStored.count)
    }

    func testeLoadComicId() {
        let model = ComicViewModel(characterId: 444, id: 1, name: "Spider Comic",
                                   description: "vai", title: "Pandemia",
                                   thumbnail: "http://", isFavorite: false)
        MarvelDataStore.saveComics(model: model)
        let selectComic =  MarvelDataStore.loadComics(id: 444)
        
        XCTAssertNotNil(selectComic)
    }

    func testeLoadComicIdNotFound() {
         let model = ComicViewModel(characterId: 444, id: 1, name: "Spider Comic",
                                    description: "vai", title: "Pandemia",
                                    thumbnail: "http://", isFavorite: false)
         MarvelDataStore.saveComics(model: model)
         let selectComic =  MarvelDataStore.loadComics(id: 44)
         
        XCTAssert(selectComic.count == 0, "Not Found")
     }
}
