////
////  MarvelDataStoreMock.swift
////  ios-challengeTests
////
////  Created by Brenno Freire Dantas on 28/04/20.
////  Copyright © 2020 Haydens. All rights reserved.
////
//
//import Foundation
//import UIKit
//import XCTest
//@testable import ios_challenge
//
//class MarvelDataStoreMock: MarvelDataStoreProtocol {
//    
//    var loadFAVCall:Bool = false
//    var addFAVCall:Bool = false
//    var delFAVFAVCall:Bool = false
//    
//    var saveComicsFAVCall:Bool = false
//    var loadComicsFAVCall:Bool = false
//    var loadComicsIdFAVCall:Bool = false
//    
//    static func loadFAV() -> [CharacterViewModel] {
//        loadFAVCall = true
//        return
//    }
//    
//    static func addFAV(model: CharacterViewModel) {
//        addFAVCall = true
//    }
//    
//    static func delFAV(model: CharacterViewModel) {
//        delFAVFAVCall = true
//    }
//    
//    static func saveComics(model: ComicViewModel) {
//        saveComicsFAVCall = true
//    }
//    
//    static func loadComics() -> [ComicViewModel] {
//        loadComicsFAVCall = true
//    }
//    
//    static func loadComics(id: Int) -> [ComicViewModel] {
//        loadComicsIdFAVCall = true
//    }
//}
