//
//  DetailsInteractorMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import XCTest
@testable import ios_challenge

class DetailsInteractorMock: DetailsInteractorProtocol {
    
    var fetchComics: Bool = false
    var fetchComicsLocal: Bool = false
    var id: Int?
    var presenter: DetailsPresenterProtocol?
    
    init(presenter: DetailsPresenterProtocol) {
        self.presenter = presenter
    }
    
    func fetchComics(id: Int) {
        fetchComics = true
        self.id = id
    }
    
    func fetchComicsLocal(id: Int) {
        fetchComicsLocal = true
        self.id = id
    }
}
