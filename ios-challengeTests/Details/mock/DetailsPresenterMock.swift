//
//  DetailsPresenterMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import XCTest
@testable import ios_challenge

class DetailsPresenterMock: DetailsPresenterProtocol {
    
    var presentComics: Bool = false
    var comics: [ComicViewModel]?
    
    func presentComics(comics: [ComicViewModel]) {
        self.comics = comics
        presentComics = true
    }
}
