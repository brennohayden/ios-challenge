
//
//  DetailsViewControllerMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import UIKit
import XCTest
@testable import ios_challenge

class DetailsViewControllerMock: DetailsViewControllerDisplay {
 
    var displayComics: Bool = false
    
    var itens: [ComicViewModel]?
    var error: Error?
    
    func displayComics(comics: [ComicViewModel]) {
        itens = comics
        displayComics = true
    }
}
