//
//  DetailsRepositoryMock.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import XCTest
@testable import ios_challenge

class DetailsRepositoryMock: DetailsRepositoryStoreProtocol {
    
    var shouldFailRequest: Bool = false
    var shouldLocalFailRequest: Bool = false
    
    func fetchComics(id: Int, onSuccess: @escaping ([Comic]) -> Void) {
        onSuccess([Comic]())
    }
    
    func fetchComicsLocal(id: Int, onSuccess: @escaping ([ComicViewModel]) -> Void) {
        onSuccess([ComicViewModel]())
    }
}
