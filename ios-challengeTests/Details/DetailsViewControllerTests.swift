//
//  DetailsViewControllerTests.swift
//  ios-challengeTests
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

@testable import ios_challenge
import XCTest

class DetailsViewControllerTests: XCTestCase {
    
    var sut: DetailsViewController!
    var spy: DetailsInteractorMock!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        sut = makeSUT()
        window = UIWindow()
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    override func tearDown() {
        window = nil
        sut = nil
        spy = nil
        super.tearDown()
    }
    
    func testComicsLocal() {
        sut = makeSUT()
        sut.isLocal = true
        sut.character = CharacterViewModel(id: 234,
                                           name: "Spider Man", description: "Bla bla",
                                           thumbnail: "http://", isFavorite: false)
        sut.viewDidLoad()
        
        XCTAssertTrue(spy.fetchComicsLocal)
    }
    
    func testComicsLocalError() {
        sut = makeSUT()
        sut.isLocal = true
        sut.character = nil
        sut.viewDidLoad()
        
        XCTAssertFalse(spy.fetchComicsLocal)
    }
    
    func testComics() {
        sut = makeSUT()
        sut.isLocal = false
        sut.character = CharacterViewModel(id: 234,
                                           name: "Spider Man", description: "Bla bla",
                                           thumbnail: "http://", isFavorite: false)
        sut.viewDidLoad()
        XCTAssertTrue(spy.fetchComics)
    }
    
    func testComicsError() {
        sut = makeSUT()
        sut.isLocal = false
        sut.character =  nil
        sut.viewDidLoad()
        XCTAssertFalse(spy.fetchComics)
    }
    
    func testViewDidAppear() {
        sut = makeSUT()
        sut.viewDidAppear(false)
        XCTAssertEqual(sut.title, nil)
    }
    
    func makeSUT() -> DetailsViewController {
        sut = DetailsViewController()
        spy = DetailsInteractorMock(presenter: DetailsPresenterMock())
        sut.interactor = spy
        return sut
    }
}
