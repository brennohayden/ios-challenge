//
//  AppDelegate.swift
//  teste
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate : UIResponder, UIApplicationDelegate {
    var window : UIWindow?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool {
        self.window?.rootViewController = MarvelRouter.routeToRootViewController()
        self.window?.makeKeyAndVisible()
        return true
    }
}
