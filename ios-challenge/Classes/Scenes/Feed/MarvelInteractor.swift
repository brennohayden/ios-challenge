//
//  MarvelInteractor.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol MarvelInteractorProtocol {
    func fetchCharacters(offset: Int, limit: Int)
}

class MarvelInteractor: MarvelInteractorProtocol {
    var repository: MarvelRepositoryStoreProtocol?
    var presenter: MarvelPresentProtocol?
    
    init(presenter: MarvelPresentProtocol) {
        self.presenter = presenter
    }
    func fetchCharacters(offset: Int, limit: Int) {
        repository?.fetchCharacters(offset: offset, limit: limit, onSuccess: { (itens) in
            var modelLocal = [CharacterViewModel]()
            let favorites = MarvelDataStore.loadFAV()
            
            itens.forEach { (character) in
                let model = CharacterViewModel(character: character)
                favorites.forEach { (item) in
                    if item.id == model.id {
                        model.isFavorite = true
                    } 
                }
                modelLocal.append(model)
            }
            self.presenter?.presentCharacters(itens: modelLocal)
        }) { (error) in
            self.presenter?.presentError(error: error)
        }
    }
}
