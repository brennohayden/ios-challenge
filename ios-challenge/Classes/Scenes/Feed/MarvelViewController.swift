//
//  MarvelViewController.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//
import Foundation
import UIKit

protocol MarvelViewControllerDisplayDelegate: class {
    func displayCharacters(itens: [CharacterViewModel])
    func displayError(error: Error)
}

class MarvelViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var interactor: MarvelInteractorProtocol?
    private var noConnectionView: NoConnectionView?
    private var displayedCharacters: [CharacterViewModel] = []
    private var currentCharacters: [CharacterViewModel] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    var limit = 30
    var offset = 0
    private var isPageRefreshing:Bool = false
    private var isSearchResults:Bool = false
    private var isErrorResults:Bool = false
    private var isNotFirstView:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Marvel"
        noConnectionView = NoConnectionView.initFromNib()
        noConnectionView?.setup(delegate: self)
        setupCollection()
        activityIndicatorView.startAnimating()
        activityIndicatorView.isHidden = false

        NotificationCenter.default.addObserver(self, selector: #selector(refreshFeed(notification:)),
                                                             name: NSNotification.Name(rawValue: "updateData"), object: nil)
        
        interactor?.fetchCharacters(offset: offset, limit: limit)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.searchBar.text = ""
        isNotFirstView = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func setupCollection() {
        self.searchBar.delegate = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.collectionViewLayout = UICollectionViewFlowLayout()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
        
        if(self.collectionView.contentOffset.y >=
            (self.collectionView.contentSize.height - self.collectionView.bounds.size.height)) {
            if !isPageRefreshing && !isSearchResults {
                isPageRefreshing = true
                self.offset += self.limit
                interactor?.fetchCharacters(offset: offset, limit: limit)
            }
        }
    }
    func emptyView() {
        if self.currentCharacters.count == 0 && !isErrorResults {
            self.collectionView.backgroundView = EmptyView.initFromNib()
            
        } else if !isErrorResults {
            self.collectionView.backgroundView = nil
        }
    }
    
    func updateData(model: CharacterViewModel) {
        self.currentCharacters.forEach { (update) in
            if update.id == model.id {
                update.isFavorite = model.isFavorite
                return
            }
        }
        DispatchQueue.main.async() {
            self.collectionView.reloadData()
        }
    }
    @objc func refreshFeed(notification: NSNotification) {

        debugPrint("Received Notification")
        
        if let dict = notification.object as? NSDictionary {
            if let newData = dict["characterModel"] as? CharacterViewModel {
                debugPrint(newData)
                updateData(model: newData)
            }
        }
    }
}

extension MarvelViewController: MarvelViewControllerDisplayDelegate {
    func displayCharacters(itens: [CharacterViewModel]) {
        isErrorResults = false
        DispatchQueue.main.async() {
            
            self.displayedCharacters.append(contentsOf: itens)
            self.currentCharacters = self.displayedCharacters
            self.isPageRefreshing = false
            
            self.collectionView.reloadData()
            self.activityIndicatorView.stopAnimating()
            self.activityIndicatorView.isHidden = true
        }
    }
    func displayError(error: Error) {
        isErrorResults = true
        self.currentCharacters.removeAll()
        self.displayedCharacters.removeAll()
        
        if error.isConnectivityError {
            DispatchQueue.main.async() {
                self.activityIndicatorView.stopAnimating()
                self.activityIndicatorView.isHidden = true
                self.collectionView.backgroundView = self.noConnectionView
                self.collectionView.reloadData()
            }
        } else {
            //TODO: Fazer error generico
            debugPrint("|||| ELSE ")
        }
    }
    
    func tryAgain() {
        limit = 30
        offset = 0

        activityIndicatorView.startAnimating()
        activityIndicatorView.isHidden = false
        interactor?.fetchCharacters(offset: offset, limit: limit)
    }
}
extension MarvelViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        emptyView()
        return currentCharacters.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MarvelCell.identifier, for: indexPath) as? MarvelCell
        cell?.viewModel = self.currentCharacters[indexPath.row]
        return cell ?? UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let charactere = self.currentCharacters[indexPath.row]
        
        if let navigationController = self.navigationController {
            self.tabBarController?.tabBar.isHidden = true
            let detailsVC = MarvelRouter.routeToDetailsViewController(charactere: charactere)
            detailsVC.setupHeaderViewDelegate(delegate: self)
            navigationController.pushViewController(detailsVC, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                         layout collectionViewLayout: UICollectionViewLayout,
                       sizeForItemAt indexPath: IndexPath) -> CGSize {
         let padding: CGFloat = 16
         let collectionCellSize = collectionView.frame.size.width - padding
         return CGSize(width: collectionCellSize/2, height: collectionCellSize/2)
    }
}

extension MarvelViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            isSearchResults = false
            currentCharacters = displayedCharacters
            searchBar.resignFirstResponder()
            collectionView.reloadData()
            return
        }
        isSearchResults = true
        currentCharacters = self.displayedCharacters.filter({ (charactere) -> Bool in
            return (charactere.name?.contains(searchText) ?? false)
        })
        self.collectionView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
extension MarvelViewController: NoConnectionViewDelegate {
    func tryAgain(view: UIView) {
        self.tryAgain()
    }
}
extension MarvelViewController: DetailsHeaderViewDelegate {
    func updateCharactre(view: DetailsHeaderView, model: CharacterViewModel) {
        updateData(model: model)
        tryAgain()
    }
}
