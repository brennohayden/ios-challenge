//
//  MarvelPresent.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol MarvelPresentProtocol {
    func presentCharacters(itens: [CharacterViewModel])
    func presentError(error: Error)
}

class MarvelPresent: MarvelPresentProtocol {
    var delegate: MarvelViewControllerDisplayDelegate?
    
    init(viewController: MarvelViewControllerDisplayDelegate) {
        delegate = viewController
    }
    func presentCharacters(itens: [CharacterViewModel]) {
        delegate?.displayCharacters(itens: itens)
    }
    func presentError(error: Error) {
        delegate?.displayError(error: error)
    }
}
