//
//  NoConnectionView.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit

protocol NoConnectionViewDelegate {
    func tryAgain(view: UIView)
}

class NoConnectionView: UIView, NibInstantiate {
    
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTryAgain: UIButton!
    var delegate: NoConnectionViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        lbTitle.text = "NO_CONNECTION".localized
        lbTryAgain.setTitle("TRY_AGAIN".localized, for: .normal)
    }
    func setup(delegate: NoConnectionViewDelegate) {
        self.delegate = delegate
    }
    @IBAction func tryAgain(_ sender: Any) {
        delegate?.tryAgain(view: self)
    }
}
