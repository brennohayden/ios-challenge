//
//  MarvelCell.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

protocol MarvelCellDelegate {
    func tappedFavorite(view: MarvelCell)
}
class MarvelCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var vmBox: UIView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    
    static var identifier: String = "MarvelCell"
    
    var viewModel: CharacterViewModel?  {
        didSet {
            guard let data = viewModel else { return }
            self.txtTitle.text = data.name
            let url = URL(string: data.thumbnail ?? String())
            bgImage.kf.indicatorType = .activity
            bgImage.kf.setImage(with: url, placeholder: UIImage(named: "marvel_placeholder"))
            if data.isFavorite {
                btnFavorite.setImage(UIImage(named: "ico_fav_full"), for: .normal)
            } else {
                btnFavorite.setImage(UIImage(named: "ico_fav"), for: .normal)
            }
            self.setupAccessibility(isFavorite: data.isFavorite)
        }
    }
    var delegate: MarvelCellDelegate?
    
    @IBAction func favoritedTapped(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.isFavorite = !viewModel.isFavorite
                
        if viewModel.isFavorite {
            btnFavorite.setImage(UIImage(named: "ico_fav_full"), for: .normal)
            MarvelDataStore.addFAV(model: viewModel)
        }else {
            btnFavorite.setImage(UIImage(named: "ico_fav"), for: .normal)
            MarvelDataStore.delFAV(model: viewModel)
        }
        let characterDic:[String: CharacterViewModel] = ["characterModel": viewModel]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: characterDic)
        delegate?.tappedFavorite(view: self)
    }
    private func setupAccessibility(isFavorite: Bool) {
        bgImage.isAccessibilityElement = false
        txtTitle.isAccessibilityElement = true
        btnFavorite.isAccessibilityElement = true
        if isFavorite {
            btnFavorite.accessibilityLabel = "FAV".localized + ", "
                + "TWO_TAP".localized + " " + "DISFAVOR".localized
         } else {
            btnFavorite.accessibilityLabel = "FAV".localized + ", "
                + "TWO_TAP".localized + " " + "FAVOR".localized
         }
    }
}
