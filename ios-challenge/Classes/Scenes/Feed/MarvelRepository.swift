//
//  MarvelRepository.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol MarvelRepositoryStoreProtocol {
    func fetchCharacters(offset: Int, limit: Int, onSuccess:@escaping (([Character])->Void), onFailure:@escaping ((Error)->Void))
}

class MarvelRepository: MarvelRepositoryStoreProtocol {
    
    func fetchCharacters(offset: Int, limit: Int, onSuccess:@escaping (([Character])->Void), onFailure:@escaping ((Error)->Void)) {
        MarvelGateway().loadServices(type: .characters(offset, limit)) { (response: Result<MarvelResponse<Character>>) in
            switch response {
            case .sucess(let characters):
                
                onSuccess(characters.data.results)
            case .failure(let error):
                onFailure(error)
            }
        }
    }
}
