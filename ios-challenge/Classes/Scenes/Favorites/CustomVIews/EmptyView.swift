//
//  EmptyView.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit


class EmptyView: UIView, NibInstantiate {
    
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        lbTitle.text = "NO_RESULTS".localized
    }
    
}
