//
//  FavoritesInteractor.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol FavoritesInteractorProtocol {
    func fetchCharacters()
}

class FavoritesInteractor: FavoritesInteractorProtocol {
    var repository: FavoritesRepositoryStoreProtocol?
    
    var presenter: FavoritesPresenterProtocol?
    
    init(presenter: FavoritesPresenterProtocol) {
        self.presenter = presenter
    }
    func fetchCharacters() {
        repository?.fetchCharacters { (itens) in
            self.presenter?.presentCharacters(itens: itens)
        }
    }
}
