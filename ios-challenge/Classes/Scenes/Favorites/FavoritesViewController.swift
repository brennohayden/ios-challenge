//
//  FavoritesViewController.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit


protocol FavoritesViewControllerDelegate: class {
    func displayCharacters(itens: [CharacterViewModel])
}

class FavoritesViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var interactor: FavoritesInteractorProtocol?
    var displayedCharacters: [CharacterViewModel]? = []
    var currentCharacters: [CharacterViewModel]? = []
    var displayComics: [ComicViewModel]? = []
    var isSearchResults:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollection()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "FAV".localized
        self.searchBar.text = ""
        self.tabBarController?.tabBar.isHidden = false
        interactor?.fetchCharacters()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func setupCollection() {
        self.searchBar.delegate = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.collectionViewLayout = UICollectionViewFlowLayout()
    }
    func emptyView() {
        if self.currentCharacters?.count == 0 {
            self.collectionView.backgroundView = EmptyView.initFromNib()
        } else {
            self.collectionView.backgroundView = nil
        }
    }
}
extension FavoritesViewController: FavoritesViewControllerDelegate {
    func displayCharacters(itens: [CharacterViewModel]) {
        self.displayedCharacters = itens
        self.currentCharacters = self.displayedCharacters
        DispatchQueue.main.async() {
            self.collectionView.reloadData()
        }
    }
}
extension FavoritesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let currentCharacters = currentCharacters else {
            self.emptyView()
            return 0
        }
        self.emptyView()
        return currentCharacters.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MarvelCell.identifier, for: indexPath) as? MarvelCell
        cell?.viewModel = self.currentCharacters?[indexPath.row]
        cell?.delegate = self
        return cell ?? UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let charactere = self.currentCharacters?[indexPath.row] else { return }
        
        if let navigationController = self.navigationController {
            self.tabBarController?.tabBar.isHidden = true
            navigationController.pushViewController(MarvelRouter.routeToDetailsViewController(charactere: charactere, isLocal: true), animated: true)
        }
    }
}

extension FavoritesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 16
        let collectionCellSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionCellSize/2, height: collectionCellSize/2)
   }
}

extension FavoritesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            isSearchResults = false
            currentCharacters = displayedCharacters
            searchBar.resignFirstResponder()
            collectionView.reloadData()
            return
        }
        isSearchResults = true
        currentCharacters = self.displayedCharacters?.filter({ (charactere) -> Bool in
            return (charactere.name?.contains(searchText) ?? false)
        })
        self.collectionView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension FavoritesViewController: MarvelCellDelegate {
    func tappedFavorite(view: MarvelCell) {
        interactor?.fetchCharacters()
    }
}
