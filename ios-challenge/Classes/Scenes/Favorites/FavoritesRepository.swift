//
//  FavoritesRepository.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol FavoritesRepositoryStoreProtocol {
    func fetchCharacters( onSuccess: @escaping ([CharacterViewModel]) -> Void)
    func fetchComics(id: String, onSuccess: @escaping ([ComicViewModel]) -> Void)
}

class FavoritesRepository: FavoritesRepositoryStoreProtocol {
    func fetchCharacters( onSuccess: @escaping ([CharacterViewModel]) -> Void) {
        onSuccess(MarvelDataStore.loadFAV())
    }
    func fetchComics(id: String, onSuccess: @escaping ([ComicViewModel]) -> Void) {
        onSuccess(MarvelDataStore.loadComics())
    }
}
