//
//  FavoritesPresenter.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol FavoritesPresenterProtocol {
    func presentCharacters(itens: [CharacterViewModel])
    func presentComics(itens: [ComicViewModel])
}

class FavoritesPresenter: FavoritesPresenterProtocol {
    var delegate: FavoritesViewControllerDelegate?
    
    init(viewController: FavoritesViewControllerDelegate) {
        delegate = viewController
    }
    func presentCharacters(itens: [CharacterViewModel]) {
        delegate?.displayCharacters(itens: itens)
    }
    func presentComics(itens: [ComicViewModel]) {
        
    }
}
