//
//  DetailsViewController.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit

protocol DetailsViewControllerDisplay: class {
    func displayComics(comics: [ComicViewModel])
}

class DetailsViewController: UIViewController {
    let screen = DetailsScreen()
    
    var interactor: DetailsInteractorProtocol?
    var character: CharacterViewModel?
    var comicsViewModel: [ComicViewModel]?
    var isLocal: Bool = false
    
    override func loadView() {
        view = screen
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "DETAILS".localized
        guard let character = character else { return }
        screen.setupModel(character: character)
        
        if isLocal {
            interactor?.fetchComicsLocal(id: character.id)
        } else {
            interactor?.fetchComics(id: character.id)
        }
    }
    func setupHeaderViewDelegate(delegate: DetailsHeaderViewDelegate) {
           screen.setupDelegate(delegate: delegate)
    }
}
extension DetailsViewController: DetailsViewControllerDisplay {
    func displayComics(comics: [ComicViewModel]) {
        self.comicsViewModel = comics
        screen.setupComics(comics: comics)
    }
}
