//
//  DetailsScreen.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit

final class DetailsScreen: UIView {
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor =  UIColor(red: 106/255, green: 12/255, blue: 11/255, alpha: 1.0)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .white
        return scrollView
    }()
    
    let maintStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = UIColor(red: 106/255, green: 12/255, blue: 11/255, alpha: 1.0)
        return stackView
    }()
    
    let detailsView = DetailsHeaderView.initFromNib()
    
    let emptyStackView: UIStackView = {
        let st = UIStackView()
        st.backgroundColor = UIColor(red: 106/255, green: 12/255, blue: 11/255, alpha: 1.0)
        return st
    }()
    
    override init(frame: CGRect = UIScreen.main.bounds) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupModel(character: CharacterViewModel) {
        detailsView.setupUI(character: character)
    }
    
    func setupComics(comics: [ComicViewModel]) {
        detailsView.setupComics(comics: comics)
    }
    func setupDelegate(delegate: DetailsHeaderViewDelegate) {
        detailsView.delegate = delegate
    }
}

extension DetailsScreen: CodeView {
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(maintStackView)
        maintStackView.addArrangedSubview(detailsView)
        maintStackView.addArrangedSubview(emptyStackView)
    }
    
    func setupConstraints() {
        scrollView.toSuperView()
        
        maintStackView.toSuperView()
        maintStackView.axis = .vertical
               
        maintStackView.anchorCenterTo(toView: scrollView)
        maintStackView.anchorSizeTo(toView: scrollView, priority: .defaultLow)
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = UIColor(red: 106/255, green: 12/255, blue: 11/255, alpha: 1.0)
    }
}
