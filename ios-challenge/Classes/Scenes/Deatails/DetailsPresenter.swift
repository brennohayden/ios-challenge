//
//  DetailsPresenter.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation


protocol DetailsPresenterProtocol {
    func presentComics(comics: [ComicViewModel])
}

class DetailsPresenter: DetailsPresenterProtocol {
    
    var delegate: DetailsViewControllerDisplay?
    
    init(viewController: DetailsViewControllerDisplay) {
        delegate = viewController
    }
    
    func presentComics(comics: [ComicViewModel]) {
        delegate?.displayComics(comics: comics)
    }
}
