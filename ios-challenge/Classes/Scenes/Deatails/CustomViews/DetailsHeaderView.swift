//
//  DetailsHeaderView.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

protocol DetailsHeaderViewDelegate {
    func updateCharactre(view: DetailsHeaderView, model: CharacterViewModel)
}
class DetailsHeaderView: UIView, NibInstantiate {
    
    @IBOutlet weak var imgHero: UIImageView!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnDoubleTap: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var lbComics: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var delegate: DetailsHeaderViewDelegate?
    var comics: [ComicViewModel]?
    private var character: CharacterViewModel?
    private var defaultCellIdentifier:String {
        get{
            return String(describing: DetailsHeaderView.self)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicatorView.startAnimating()
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        btnDoubleTap.addGestureRecognizer(tap)
        configureCarousel()
    }
    func setupUI(character: CharacterViewModel) {
        self.character = character
        lbComics.text = "COMICS".localized
        let url = URL(string: character.thumbnail ?? String())
        self.imgHero.kf.setImage(with: url)
        lbTitle.text = character.name
        lbDesc.text = character.desc
        if character.isFavorite {
            btnFavorite.setImage(UIImage(named: "ico_fav_full"), for: .normal)
        } else {
            btnFavorite.setImage(UIImage(named: "ico_fav"), for: .normal)
        }
        accessibility(isFavorite: character.isFavorite , title: character.name ?? "" )
    }
    func setupComics(comics: [ComicViewModel]) {
        self.comics = comics
        DispatchQueue.main.async() {
            self.activityIndicatorView.stopAnimating()
            self.activityIndicatorView.isHidden = true
            self.collectionView.reloadData()
        }
    }
    
    @objc func doubleTapped() {
        self.favorite()
    }
    
    @IBAction func favoritedTapped(_ sender: Any) {
        self.favorite()
    }
    
    private func favorite() {
        guard let character = character else { return }
        character.isFavorite = !character.isFavorite
        if character.isFavorite {
            MarvelDataStore.addFAV(model: character)
            btnFavorite.setImage(UIImage(named: "ico_fav_full"), for: .normal)
        } else {
            MarvelDataStore.delFAV(model: character)
            btnFavorite.setImage(UIImage(named: "ico_fav"), for: .normal)
        }
        
        self.comics?.forEach({ (comics) in
            comics.characterId = character.id
            comics.isFavorite = character.isFavorite
            MarvelDataStore.saveComics(model: comics)
        })
        delegate?.updateCharactre(view: self, model: character)
    }
    func configureCarousel() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib.init(nibName: "ComicCell", bundle: Bundle(for: type(of: self))), forCellWithReuseIdentifier: defaultCellIdentifier)
    }
    
    func accessibility(isFavorite: Bool, title: String) {
        imgHero.isAccessibilityElement = false
        
        if isFavorite {
           btnFavorite.accessibilityLabel = "FAV".localized + ", "
               + "TWO_TAP".localized + " " + "DISFAVOR".localized
            btnDoubleTap.accessibilityLabel = "Favoritos".localized + ", "
                + "TWO_TAP".localized + " " + "DISFAVOR".localized + title
        } else {
           btnFavorite.accessibilityLabel = "FAV".localized + ", "
               + "TWO_TAP".localized + " " + "FAVOR".localized
            btnDoubleTap.accessibilityLabel = "Favoritos".localized + ", "
                + "TWO_TAP".localized + " " + "FAVOR".localized + title
        }
    }
}
extension DetailsHeaderView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.comics?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:
            defaultCellIdentifier, for: indexPath) as? ComicCell else { return UICollectionViewCell() }
        cell.viewModel = self.comics?[indexPath.row]
        return cell
    }
}

extension DetailsHeaderView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 175)
   }
}

