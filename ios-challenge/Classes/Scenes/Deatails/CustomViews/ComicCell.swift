//
//  ComicCell.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit

class ComicCell: UICollectionViewCell {
    
    @IBOutlet weak var commomImage: UIImageView!
    @IBOutlet weak var commomName: UILabel!

    
    var viewModel: ComicViewModel?  {
        didSet {
            guard let data = viewModel else { return }
            self.commomName.text = data.title ?? String()
            let url = URL(string: data.thumbnail ?? String())
            commomImage.kf.indicatorType = .activity
            commomImage.kf.setImage(with: url, placeholder: UIImage(named: "comic_placeholder"))
        }
    }
}
