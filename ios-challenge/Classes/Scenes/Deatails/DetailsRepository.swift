//
//  DetailsRepository.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol DetailsRepositoryStoreProtocol {
    func fetchComics(id: Int, onSuccess: @escaping ([Comic]) -> Void)
    func fetchComicsLocal(id: Int, onSuccess: @escaping ([ComicViewModel]) -> Void)
}

class DetailsRepository: DetailsRepositoryStoreProtocol {
    func fetchComics(id: Int, onSuccess: @escaping ([Comic]) -> Void) {
        MarvelGateway().loadServices(type: .comics(id)) { (response: Result<MarvelResponse<Comic>>) in
            switch response {
            case .sucess(let reponse):
                let results = reponse.data.results
                onSuccess(results)
            case .failure(let error):
                //TODO: connection
                debugPrint("||||\(error)")
            }
        }
    }
    
    func fetchComicsLocal(id: Int, onSuccess: @escaping ([ComicViewModel]) -> Void) {
        onSuccess(MarvelDataStore.loadComics(id: id))
    }
}
