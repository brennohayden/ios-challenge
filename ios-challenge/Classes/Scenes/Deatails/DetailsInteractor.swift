//
//  DetailsInteractor.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation


protocol DetailsInteractorProtocol {
    func fetchComics(id: Int)
    func fetchComicsLocal(id: Int)
}

class DetailsInteractor: DetailsInteractorProtocol {
    var presenter: DetailsPresenterProtocol?
    var repository: DetailsRepositoryStoreProtocol? = DetailsRepository()
    
    init(presenter: DetailsPresenterProtocol) {
        self.presenter = presenter
    }
    
    func fetchComics(id: Int) {
        repository?.fetchComics(id: id) { (comics) in
            var viewModel = [ComicViewModel]()
            comics.forEach( { viewModel.append(ComicViewModel(comic: $0)) })
            self.presenter?.presentComics(comics: viewModel)
        }
    }
    func fetchComicsLocal(id: Int) {
        repository?.fetchComicsLocal(id: id) { (comics) in
            self.presenter?.presentComics(comics: comics)
        }
    }
}
