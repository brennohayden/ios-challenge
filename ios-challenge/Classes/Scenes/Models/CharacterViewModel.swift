//
//  HeroViewModel.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

class CharacterViewModel: Codable {
    static var defaultIdentifier:String {
        get{
            return String(describing: CharacterViewModel.self)
        }
    }
    
    let id: Int
    let name: String?
    let desc: String?
    let thumbnail: String?
    var isFavorite: Bool = false
    
    init(id: Int, name: String, description: String, thumbnail: String, isFavorite: Bool) {
        self.id = id
        self.name = name
        self.desc = description
        self.thumbnail = thumbnail
        self.isFavorite = isFavorite
    }
    
    init(character: Character) {
        let path = character.thumbnail?.path ?? String()
        let ext = character.thumbnail?.thumbnailExtension?.rawValue ?? String()
        self.id = character.id
        self.name = character.name
        self.desc = character.description
        self.thumbnail = path + "." + ext
    }
}
