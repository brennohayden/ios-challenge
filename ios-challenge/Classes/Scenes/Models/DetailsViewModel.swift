//
//  DetailsViewModel.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

class ComicViewModel: Codable {
    
    static var defaultIdentifier:String {
        get{
            return String(describing: ComicViewModel.self)
        }
    }
    var characterId: Int
    let id: Int
    let name: String?
    let description: String?
    let title: String?
    let thumbnail: String?
    var isFavorite: Bool = false
    
    init(characterId:Int, id: Int, name: String, description: String, title: String, thumbnail: String, isFavorite: Bool) {
        self.characterId = id
        self.id = id
        self.name = name
        self.description = description
        self.title = title
        self.thumbnail = thumbnail
        self.isFavorite = isFavorite
    }
    
    init(comic: Comic) {
        let path = comic.thumbnail?.path ?? String()
        let ext = comic.thumbnail?.thumbnailExtension?.rawValue ?? String()
        
        self.id = comic.id
        self.name = comic.name
        self.description = comic.description
        self.title = comic.title
        self.thumbnail = path + "." + ext
        self.characterId = 0
    }
}
