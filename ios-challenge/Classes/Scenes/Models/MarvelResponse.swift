//
//  MarvelResponse.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

struct MarvelResponse<T: Decodable>: Decodable {
    let data: MarvelData<T>
}

struct MarvelData<T: Decodable>: Decodable {
    let results: [T]
}
//----
struct Character: Decodable {
    let id: Int
    let digitalID: Int?
    let name: String?
    let description: String?
    let title: String?
    let thumbnail: Thumbnail?

    enum CodingKeys: String, CodingKey {
        case digitalID = "digitalId"
        case description, id, name, title, thumbnail
    }
}
struct Comic: Decodable {
    let id: Int
    let digitalID: Int?
    let name, description, title: String?
    let thumbnail: Thumbnail?

    enum CodingKeys: String, CodingKey {
        case digitalID = "digitalId"
        case description, id, name, title, thumbnail
    }
}

struct Series: Decodable {
    let id: Int
    let digitalID: Int?
    let name, description, title: String?
    let thumbnail: Thumbnail?

    enum CodingKeys: String, CodingKey {
        case digitalID = "digitalId"
        case description, id, name, title, thumbnail
    }
}

struct Thumbnail: Decodable {
    let path: String?
    let thumbnailExtension: Extension?

    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

enum Extension: String, Decodable {
    case gif = "gif"
    case jpg = "jpg"
    case png = "png"
}
