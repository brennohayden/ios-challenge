//
//  MarvelRouter.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import UIKit

class MarvelRouter {

    static var tabBarController: UITabBarController?
    
    static func routeToRootViewController() -> UIViewController {
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [feedViewController(), favoritiesViewController()]
        self.tabBarController = tabBarController
        return tabBarController
    }
    
    static func routeToDetailsViewController(charactere: CharacterViewModel, isLocal:Bool = false) -> DetailsViewController  {
        let viewController = DetailsViewController()
        let presenter = DetailsPresenter(viewController: viewController)
        let interactor = DetailsInteractor(presenter: presenter)
               
        viewController.interactor = interactor
        viewController.isLocal = isLocal
        viewController.modalPresentationStyle = .fullScreen
        viewController.character = charactere
        return viewController
    }
    
    private static func feedViewController() -> UINavigationController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MarvelViewController") as! MarvelViewController
        viewController.tabBarItem = UITabBarItem(tabBarSystemItem: .mostRecent, tag: 0)
        let presenter = MarvelPresent(viewController: viewController)
        let interactor = MarvelInteractor(presenter: presenter)
        interactor.repository = MarvelRepository()
        viewController.interactor = interactor
        
        return UINavigationController(rootViewController: viewController)
    }
    
    private static func favoritiesViewController() -> UINavigationController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
        viewController.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
        let presenter = FavoritesPresenter(viewController: viewController)
        let interactor = FavoritesInteractor(presenter: presenter)
        interactor.repository = FavoritesRepository()
        viewController.interactor = interactor
        return UINavigationController(rootViewController: viewController)
    }
}
