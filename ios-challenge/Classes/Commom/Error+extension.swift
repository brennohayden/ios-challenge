//
//  Error+extension.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit

extension Error {

    var isConnectivityError: Bool {
        let code = (self as NSError).code

        if (code == NSURLErrorTimedOut) {
            return true // time-out
        }
        if (self._domain != NSURLErrorDomain) {
            return false // Cannot be a NSURLConnection error
        }
        switch (code) {
        case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost, NSURLErrorCannotConnectToHost:
            return true
        default:
            return false
        }
    }
}
