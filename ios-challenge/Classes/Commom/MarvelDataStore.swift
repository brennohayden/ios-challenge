//
//  MarvelDataStore.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 27/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

protocol MarvelDataStoreProtocol {
    static func loadFAV() -> [CharacterViewModel]
    static func addFAV(model: CharacterViewModel)
    static func delFAV(model: CharacterViewModel)
    static func saveComics(model: ComicViewModel)
    static func loadComics() -> [ComicViewModel]
    static func loadComics(id: Int) -> [ComicViewModel]
}
class MarvelDataStore: MarvelDataStoreProtocol {
    private static let FAV = "FAV"
    
    static func loadFAV() -> [CharacterViewModel] {
        if let data = UserDefaults.standard.data(forKey: FAV) {
            do {
                let decoder = JSONDecoder()
                return try decoder.decode([CharacterViewModel].self, from: data)
            } catch {
                print("Unable to Decode Notes (\(error))")
            }
        }
        return  [CharacterViewModel]()
    }
    
    static func addFAV(model: CharacterViewModel) {
        var listFav = loadFAV()
        listFav.append(model)
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(listFav)
            UserDefaults.standard.set(data, forKey: FAV)
        } catch {
            print("Unable to Encode Array of \(CharacterViewModel.defaultIdentifier) (\(error))")
        }
    }
    static func delFAV(model: CharacterViewModel) {
        var listFav = loadFAV()
        listFav.removeAll(where: { $0.id == model.id })
        
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(listFav)
            UserDefaults.standard.set(data, forKey: FAV)
        } catch {
            print("Unable to Encode Array of \(CharacterViewModel.defaultIdentifier) (\(error))")
        }
    }
    //MARK: ComicViewModel
    
    static func saveComics(model: ComicViewModel) {
        var comics = self.loadComics()
        comics.removeAll(where: { $0.id == model.id })
        if model.isFavorite {
            comics.append(model)
        }
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(comics)
            UserDefaults.standard.set(data, forKey: ComicViewModel.defaultIdentifier)
        } catch {
            print("Unable to Encode Array of \(CharacterViewModel.defaultIdentifier) (\(error))")
        }
    }
    
    static func loadComics() -> [ComicViewModel] {
        if let data = UserDefaults.standard.data(forKey: ComicViewModel.defaultIdentifier) {
            do {
                let decoder = JSONDecoder()
                let comics = try decoder.decode([ComicViewModel].self, from: data)
                return comics
            } catch {
                print("Unable to Decode Notes (\(error))")
            }
        }
        return [ComicViewModel]()
    }
    
    static func loadComics(id: Int) -> [ComicViewModel] {
        let comics = self.loadComics()
        let results = comics.filter( { $0.characterId == id })
        
        return results
    }
}
