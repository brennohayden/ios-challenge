//
//  String+Localization.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 28/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

