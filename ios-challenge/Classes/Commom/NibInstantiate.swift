//
//  NibInstantiate.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation

public protocol NibInstantiate {
    static var nibName: String { get}
}

extension NibInstantiate {
    public static var nibName: String {
        return String(describing: self)
    }
}
