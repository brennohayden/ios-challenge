//
//  MarvelGateway.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

enum MarvelRequestType {
    case characters(Int, Int)
    case comics(Int)
}

protocol RequestGateway {
    func loadServices<T: Decodable>(type: MarvelRequestType, onComplete: @escaping (Result<T>) -> Void)
}

public class MarvelGateway: RequestGateway {
    
    var environment: [String:AnyObject]? {
        get {
            return Bundle.main.infoDictionary?["ServerConfiguration"] as? [String:AnyObject]
        }
    }
    var host: String {
        get {
            return environment?["baseURL"] as? String ?? ""
        }
    }
    var apikey: String {
        get {
            return environment?["apiKey"] as? String ?? ""
        }
    }
    var privateKey: String {
        get {
            return (environment?["privateKey"] as? String) ?? ""
        }
    }
    let ts = Date().timeIntervalSince1970.description
    
    var hash: String {
        return "\(ts)\(privateKey)\(apikey)".md5()
    }
    var parameters: String {
        return "apikey=\(apikey)&ts=\(ts)&hash=\(hash)"
    }
    func loadServices<T: Decodable>(type: MarvelRequestType, onComplete: @escaping (Result<T>) -> Void) {
        switch type {
        case .characters(let page, let limite):
            guard let url = URL(string: host + "/characters?limit=\(limite)&offset=\(page)&\(parameters)") else { return }
            RequestAdapter.request(url, onComplete)
        case .comics(let id):
            guard let url = URL(string: host + "characters/\(id)/comics?\(parameters)") else { return }
            RequestAdapter.request(url, onComplete)
        }
    }

}
