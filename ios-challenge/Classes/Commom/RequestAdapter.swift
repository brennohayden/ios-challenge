//
//  RequestAdapter.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 26/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit

enum Result<T> {
    case sucess(T)
    case failure(Error)
}

class RequestAdapter {
    static func request<T: Decodable>(_ url: URL, _ onComplete: @escaping (Result<T>) -> Void) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 300
        
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: url) { (data, _, requestError) in
            if let requestError = requestError  {
                return onComplete(.failure(requestError))
            }
            guard let data = data else { return }
            do {
                let response = try JSONDecoder().decode(T.self, from: data)
                onComplete(.sucess(response))
            } catch {
                onComplete(.failure(error))
            }
        }
        task.resume()
    }
}
