//
//  UIView+extension.swift
//  ios-challenge
//
//  Created by Brenno Freire Dantas on 25/04/20.
//  Copyright © 2020 Haydens. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    func toSuperView() {
        translatesAutoresizingMaskIntoConstraints = false

        if let superview = superview {
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
            leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
        }
    }

    func anchorCenterTo(toView: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: toView.centerXAnchor).isActive = true

    }
    func anchorSizeTo(toView: UIView, priority: UILayoutPriority) {
        let constraint = self.heightAnchor.constraint(equalTo: toView.heightAnchor)
        constraint.priority = priority
        constraint.isActive = true
    }
}

public extension NibInstantiate where Self: UIViewController {
    static func initFromNib() -> Self {
        return Self(nibName: nibName, bundle: Bundle(for: self))
    }
}
public extension NibInstantiate where Self: UIView { static func initFromNib() -> Self {
    return UINib(nibName: nibName, bundle: Bundle(for: self)).instantiate(withOwner: self, options: nil)[0] as! Self
    }
}
